/*Teste automatizado feito em Cypress, um FrameWork do Node.js
Caso não tenho o Cypress instalado você primeiro precisa ter o Node.js instalado
Após instalar o Node execute o seguinte comando no terminal(pode ser instalado direto do 
   terminal do Visual studio Code), segue o comando para instalar: npm install cypress
Após instalar o Cypress execute no terminal no comando: ./node_modules/.bin/cypress open
concluído esse processo o código está pronto para ser executado*/

/*outro ponto importante é que para ficar mais legível, foi usado locators locais
ou seja, tudo feito no mesmo arquivo js assim sendo mais fácil executar atráves do git lab*/

const { select, reject } = require("async");


describe('Primeiro teste em um aplicação "real"', () => {
//Html fornecido no curso de cypress da Udemy
   //cy.visit('https://barrigareact.wcaquino.me/');

 it('Criando uma conta', ()=>{
     //visita a página de login
    cy.visit('https://barrigareact.wcaquino.me/');

    //clicar no botão Registrar
    cy.get(':nth-child(2) > .nav-link').click();
    
    //caso alguém esteja lendo e tentando executar esse código, subistitua meu nome, email e senha por outra qualquer
    //Preenche o primeiro input com o nome Raul
    cy.get('.jumbotron > :nth-child(1) > .form-control').type('Raul');
    
    //Preenche o segundo input com o email, neste caso raulacacio@hotmail.com
    cy.get('.input-group > .form-control').type('raulacacio@hotmail.com')

    /*Preenche o último input com uma senha, nesse caso uma bem padrão "123456", 
    Como esse HTML é somente para aprendeizado, a senha não tem nenhum tipo de validação,
    como por exemplo uma letra maiúscula ou composta por letras e números*/
    cy.get(':nth-child(3) > .form-control').type('123456')
    
    //E por fim clica no botão de registrar
    cy.get('.btn').click()
    
    /*Aqui foi necessário um wait por conta do meu notebook lento, provavelmente 
    pra você não será necessario*/
    cy.wait(2000);

    /*Assim encerrando a primeira parte do teste, um teste simples de criação de conta,
    não levando em consideração tamanho da fonte de letras, margem, espaçamentos ou responsividade*/

    //Após o cadastro é esperado uma mensagem positiva do cadastro ou de erro
    //Como a conta já exite a mensagem apresentada é de erro
    cy.get('.toast-message').should('contain', 'Erro: Error: Request failed with status code 500');

    //A mensagem de Bem vindo só é exibida caso a conta sejá nova
    //cy.get('.toast-message').should('contain', 'Bem vindo');
 })

 it('Acessar a conta', ()=>{
   //Para evitar algum "lixo" na página foi dado um reload
    cy.reload();
    
    //Acessar a página de login
    cy.visit('https://barrigareact.wcaquino.me/');

   //Inseri o email utilizado na criação da conta
   cy.get('.input-group > .form-control').type('raulacacio@hotmail.com');
   
   //Inseri a senha
   cy.get(':nth-child(2) > .form-control').type('123456');

   //Clica no botão 'Entrar'
   cy.get('.btn').click();

   //Após o acesso da conta o usuário deve receber uma mensagem de Bem vindo, um alert
   cy.get('.toast-message').should('contain', 'Bem vindo');

   cy.get('[data-test = "menu-settings"')
   .click();
   cy.get('[href="/contas"]')
   .click();

 })

 it('Inserir conta', ()=>{
   //O próximo passo do desafio é inserir uma conta
   //Não foi a melhor conclusão para inserir uma conta, preciso refazer
   //o Cypress vai clicar no dropdonw e em seguida clicar em Contas para adicionar uma nova conta
   cy.get('[data-test = "menu-settings"')
   .click();
   cy.get('[href="/contas"]')
   .click();
   
   //vai preencher o campor com umm nome essa conta
   cy.get('.form-control').type('Luz');

   //logo após inserir o nome, vai clicar no botão de armazenamento
   cy.get('.btn').click();
   
   //Caso a conta não estejá repetida você recebera uma mensagem de sucesso
   cy.get('.toast-message').should('contain', 'sucesso');

   //Para validar a mensagem de erro, vamos inserir uma conta com o mesmo nome
   cy.get('.form-control').type('Luz');
   cy.get('.btn').click();

   //Teste de validação da mensagem de erro
   cy.get('.toast-message').should('contain', 'Error');

 })

 it('Desafio de alteração de conta', ()=>{
  //Aqui foi usando o locator do xpath para chegar ao ícone correspondente a conta de nome Luz
  cy.xpath("//table//td[contains(.,'Luz')]/..//i[@class='far fa-edit']").click();
  
  //Limpa o campo para não haver nada digitado
  cy.get('.form-control').clear();
  //Altera o nome da conta
  cy.get('.form-control').type('luzPaga');
  
  //Foi necassario um wait aqui, Revisar depois 
  cy.wait(1000)
  
  //clica pra salvar
  cy.get('.btn').click();

  //Receber uma mensagem de confirmação da alteração
  cy.get('.toast-message').should('contain', 'sucesso');

  
 })

 it('Inserir um conta repetida e verificar a mensagem de erro', ()=>{
  //Limpa o campo para inserir o nome da conta repetido
   cy.get('.form-control').clear();

   //Inseri o nome repetido 
   cy.get('.form-control').type('luzPaga');

   cy.wait(1000);

   //clica pra salvar
  cy.get('.btn').click();

  //Verifica a mensagem de erro
  cy.get('.toast-message').should('contain', 'Error');

 })

 it('Inseir uma movimentação', ()=>{

  //Primeiramente o cypress vai clicar no botão de menu para a adição de valores
  cy.get('[data-test=menu-movimentacao] > .fas').click();
 
  //Edita o input de descrição e adiciona uma frase
  cy.get('[data-test=descricao]').type('Conta de luz paga');

  //Edita o input de valores e adiciona um valor
  //Obs: lembrando que esse teste não é pra verificafr se a página aceitaria por exemplo uma string no lugar dos números
  cy.get('[data-test=valor]').type(120);

  //Mais um input para editar
  cy.get('[data-test=envolvido]').type('A mim');

  //clica para alterar o status da conta
  cy.get('[data-test=status]').click();

  //E por fim salva o que foi adicionado
  cy.get('.btn-primary').click();

  //verifica se a mensagem foi de sucesso
  cy.get('.toast-message').should('contain', 'sucesso');

  //como última verificação, procuramos na lista de contas o nome que editamos para ver se foi inserido de fato
  cy.xpath("//span[contains(.,'Conta de luz paga')]").should('exist');
 })

 it('Remover movimentação', ()=>{
   // Lembrando que como é uma movimentação única, não fiz um locator com o xpath$ git add .
  //clica no ícone de remoção de uma movimentação
  cy.get('.col > .far').click();

  //valida a mensagem de remoção
  cy.get('.toast-message').should('contain', 'Movimentação removida');
 })


})

